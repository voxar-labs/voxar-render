#include <osgViewer/Viewer>
#include <osg/GraphicsContext>
#include <osg/Camera>
#include <osg/Viewport>
#include <osg/StateSet>
#include <osg/Program>
#include <osg/Shader>
#include <osgUtil/Optimizer>

#include <osg/Geode>
#include <osg/ShapeDrawable>

#include <ifcpp/reader/IfcPPReaderSTEP.h>
#include <ifcpp/geometry/GeometryConverter.h>

#include <iostream>
#include <string>

osg::ref_ptr<osg::Geode> createSceneGraph(const std::vector <float> &vertices, const std::vector <float> &normals, const std::vector <float> &colors)
{
	// Create an object to store geometry in.
	osg::ref_ptr<osg::Geometry> geom = new osg::Geometry;
	// Create an array of four vertices.
	osg::ref_ptr<osg::Vec3Array> v = new osg::Vec3Array;
	geom->setVertexArray(v.get());
	for (int i = 0; i < vertices.size(); i += 3){
		v->push_back(osg::Vec3(vertices.at(i), vertices.at(i + 1), vertices.at(i + 2)));
	}
	// Create an array of four colors.
	osg::ref_ptr<osg::Vec4Array> c = new osg::Vec4Array;
	geom->setColorArray(c.get());
	geom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
	for (int i = 0; i < colors.size(); i += 4){
		c->push_back(osg::Vec4(colors.at(i), colors.at(i + 1), colors.at(i + 2), colors.at(i + 3)));
	}

	// Create an array for the single normal.
	osg::ref_ptr<osg::Vec3Array> n = new osg::Vec3Array;
	geom->setNormalArray(n.get());
	geom->setNormalBinding(osg::Geometry::BIND_PER_VERTEX);
	for (int i = 0; i < normals.size(); i += 3){
		n->push_back(osg::Vec3(normals.at(i), normals.at(i + 1), normals.at(i + 2)));
	}
	// Draw a four-vertex quad from the stored data.
	geom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::TRIANGLES, 0, vertices.size() / 3));

	// Add the Geometry (Drawable) to a Geode and
	// return the Geode.
	osg::ref_ptr<osg::Geode> geode = new osg::Geode;
	geode->addDrawable(geom.get());
	return geode.get();
}


void configureShaders(osg::StateSet* stateSet)
{
	const std::string vertexSource =
		"#version 140 \n"
		" \n"
		"uniform mat4 osg_ModelViewProjectionMatrix; \n"
		"uniform mat3 osg_NormalMatrix; \n"
		"uniform vec3 ecLightDir; \n"
		" \n"
		"in vec4 osg_Vertex; \n"
		"in vec3 osg_Color; \n"
		"in vec3 osg_Normal; \n"
		"out vec4 color; \n"
		" \n"
		"void main() \n"
		"{ \n"
		"    vec3 ecNormal = normalize( osg_NormalMatrix * osg_Normal ); \n"
		"    float diffuse = max( dot( ecLightDir, ecNormal ), 0. ); \n"
		"    color = vec4( osg_Color*diffuse, 1. ); \n"
		" \n"
		"    gl_Position = osg_ModelViewProjectionMatrix * osg_Vertex; \n"
		"} \n";
	osg::Shader* vShader = new osg::Shader(osg::Shader::VERTEX, vertexSource);

	const std::string fragmentSource =
		"#version 140 \n"
		" \n"
		"in vec4 color; \n"
		"out vec4 fragData; \n"
		" \n"
		"void main() \n"
		"{ \n"
		"    fragData = color; \n"
		"} \n";
	osg::Shader* fShader = new osg::Shader(osg::Shader::FRAGMENT, fragmentSource);

	osg::Program* program = new osg::Program;
	program->addShader(vShader);
	program->addShader(fShader);
	stateSet->setAttribute(program);

	osg::Vec3f lightDir(0., 0.5, 1.);
	lightDir.normalize();
	stateSet->addUniform(new osg::Uniform("ecLightDir", lightDir));
}

int main(int argc, char** argv)
{
	std::string filepath;
	std::cout << "Type the file's relative path..." << std::endl;
	std::cin >> filepath;

	if (!std::cin)
		return 1;
	
	IfcPPReaderSTEP reader;
	shared_ptr<IfcPPModel> model(new IfcPPModel());
	reader.loadModelFromFile( std::wstring(filepath.begin(), filepath.end()), model );
	
	GeometryConverter geometryConverter(model);
	osg::ref_ptr<osg::Switch> parent_group = new osg::Switch();
	geometryConverter.createGeometryOSG(parent_group);


	osgUtil::Optimizer optimizer;
	optimizer.optimize(parent_group.get(), osgUtil::Optimizer::ALL_OPTIMIZATIONS | osgUtil::Optimizer::TESSELLATE_GEOMETRY);

	configureShaders(parent_group->getOrCreateStateSet());

	const int width(1280), height(720);
	const std::string version("3.1");
	osg::ref_ptr< osg::GraphicsContext::Traits > traits = new osg::GraphicsContext::Traits();
	traits->x = 20; traits->y = 30;
	traits->width = width; traits->height = height;
	traits->windowDecoration = true;
	traits->doubleBuffer = true;
	traits->glContextVersion = version;
	osg::ref_ptr< osg::GraphicsContext > gc = osg::GraphicsContext::createGraphicsContext(traits.get());
	if (!gc.valid())
	{
		osg::notify(osg::FATAL) << "Unable to create OpenGL v" << version << " context." << std::endl;
		return(1);
	}

	osgViewer::Viewer viewer;

	// Create a Camera that uses the above OpenGL context.
	osg::Camera* cam = viewer.getCamera();
	cam->setGraphicsContext(gc.get());
	// Must set perspective projection for fovy and aspect.
	cam->setProjectionMatrix(osg::Matrix::perspective(30., (double)width / (double)height, 1., 100.));
	// Unlike OpenGL, OSG viewport does *not* default to window dimensions.
	cam->setViewport(new osg::Viewport(0, 0, width, height));

	viewer.setSceneData(parent_group);

	// for non GL3/GL4 and non GLES2 platforms we need enable the osg_ uniforms that the shaders will use,
	// you don't need thse two lines on GL3/GL4 and GLES2 specific builds as these will be enable by default.
	gc->getState()->setUseModelViewAndProjectionUniforms(true);
	gc->getState()->setUseVertexAttributeAliasing(true);

	return(viewer.run());
}
