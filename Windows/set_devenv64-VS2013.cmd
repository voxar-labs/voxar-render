set OSG_DIR=J:\temp\OpenSceneGraph-3.2.1\out
set OSG_LIBRARY_PATH=%OSG_DIR%\bin
set OSG_PLUGINS_LIB=%OSG_DIR%\lib\osgPlugins-3.2.1
set OSG_FILE_PATH=J:\temp\OpenSceneGraph-Data-master;J:\temp\OpenSceneGraph-Data-master\Images;J:\temp\OpenSceneGraph-Data-master\fonts;

set BOOST_DIR=J:\temp\ifcplusplus-master\dependencies\Boost\include
set CGAL_DIR=C:\Program Files\CGAL-4.3\auxiliary\gmp\lib

set QMAKESPEC=win32-msvc2013
set QMAKE_TARGET.arch=x86_64
set QTDIR=C:\Qt\Qt5.4.0\5.4\msvc2013_opengl

PATH=%OSG_DIR%\bin;%PATH%
PATH=%OSG_PLUGINS_LIB%;%PATH%
PATH=%QTDIR%\bin;%PATH%
PATH=%CGAL_DIR%\bin;%PATH%
PATH=C:\Program Files (x86)\WiX Toolset v3.8\bin;%PATH%

call "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" x86_amd64

devenv

