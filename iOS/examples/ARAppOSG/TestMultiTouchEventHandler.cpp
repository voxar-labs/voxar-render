//
//  TestMultiTouchEventHandler.cpp
//  OSGSampleIOS
//
//  Created by Matheus Coelho Berger on 9/14/16.
//
//

#include "TestMultiTouchEventHandler.hpp"


void TestMultiTouchEventHandler::createTouchRepresentations(osg::Group* parent_group, unsigned int num_objects)
{
    // create some geometry which is shown for every touch-point
    for(unsigned int i = 0; i != num_objects; ++i)
    {
        std::ostringstream ss;
        
        osg::Geode* geode = new osg::Geode();
        
        osg::ShapeDrawable* drawable = new osg::ShapeDrawable(new osg::Box(osg::Vec3(0,0,0), 100));
        //            drawable->setColor(osg::Vec4(0.5, 0.5, 0.5,1));
        geode->addDrawable(drawable);
        
        ss << "Touch " << i;
        
        osgText::Text* text = new  osgText::Text;
        geode->addDrawable( text );
        drawable->setDataVariance(osg::Object::DYNAMIC);
        _drawables.push_back(drawable);
        
        
        text->setFont("fonts/arial.ttf");
        text->setPosition(osg::Vec3(110,0,0));
        text->setText(ss.str());
        _texts.push_back(text);
        text->setDataVariance(osg::Object::DYNAMIC);
        
        
        
        osg::MatrixTransform* mat = new osg::MatrixTransform();
        mat->addChild(geode);
        mat->setNodeMask(0x0);
        
        _mats.push_back(mat);
        
        parent_group->addChild(mat);
    }
    
    parent_group->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
}

bool TestMultiTouchEventHandler::handle (const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa, osg::Object *, osg::NodeVisitor *)
{
    switch(ea.getEventType())
    {
        case osgGA::GUIEventAdapter::FRAME:
            if (_cleanupOnNextFrame) {
                cleanup(0);
                _cleanupOnNextFrame = false;
            }
            break;
            
        case osgGA::GUIEventAdapter::PUSH:
        case osgGA::GUIEventAdapter::DRAG:
        case osgGA::GUIEventAdapter::RELEASE:
        {
            // is this a multi-touch event?
            if (!ea.isMultiTouchEvent())
                return false;
            
            unsigned int j(0);
            
            // iterate over all touch-points and update the geometry
            unsigned num_touch_ended(0);
            
            for(osgGA::GUIEventAdapter::TouchData::iterator i = ea.getTouchData()->begin(); i != ea.getTouchData()->end(); ++i, ++j)
            {
                const osgGA::GUIEventAdapter::TouchData::TouchPoint& tp = (*i);
                if (ea.getMouseYOrientation() == osgGA::GUIEventAdapter::Y_INCREASING_DOWNWARDS)
                    _mats[j]->setMatrix(osg::Matrix::translate(tp.x, -(ea.getWindowHeight() - tp.y), 0));
                else
                    _mats[j]->setMatrix(osg::Matrix::translate(tp.x, -tp.y, 0));
                
                _mats[j]->setNodeMask(0xffff);
                
                std::ostringstream ss;
                ss << "Touch " << tp.id;
                _texts[j]->setText(ss.str());
                
                switch (tp.phase)
                {
                    case osgGA::GUIEventAdapter::TOUCH_BEGAN:
                        _drawables[j]->setColor(osg::Vec4(0,1,0,1));
                        std::cout << "touch began: " << ss.str() << std::endl;
                        break;
                        
                    case osgGA::GUIEventAdapter::TOUCH_MOVED:
                        //std::cout << "touch moved: " << ss.str() << std::endl;
                        _drawables[j]->setColor(osg::Vec4(1,1,1,1));
                        break;
                        
                    case osgGA::GUIEventAdapter::TOUCH_ENDED:
                        _drawables[j]->setColor(osg::Vec4(1,0,0,1));
                        std::cout << "touch ended: " << ss.str() << std::endl;
                        ++num_touch_ended;
                        break;
                        
                    case osgGA::GUIEventAdapter::TOUCH_STATIONERY:
                        _drawables[j]->setColor(osg::Vec4(0.8,0.8,0.8,1));
                        break;
                        
                    default:
                        break;
                        
                }
            }
            
            // hide unused geometry
            cleanup(j);
            
            //check if all touches ended
            if ((ea.getTouchData()->getNumTouchPoints() > 0) && (ea.getTouchData()->getNumTouchPoints() == num_touch_ended))
            {
                _cleanupOnNextFrame = true;
            }
            
            // reposition mouse-pointer
            aa.requestWarpPointer((ea.getWindowX() + ea.getWindowWidth()) / 2.0, (ea.getWindowY() + ea.getWindowHeight()) / 2.0);
        }
            break;
            
        default:
            break;
    }
    
    return false;
}

