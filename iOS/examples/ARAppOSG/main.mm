//
//  main.m
//  iphoneViewer
//
//  Created by Thomas Hogarth on 10/05/2009.
//  Copyright HogBox 2009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppOSGDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppOSGDelegate class]));
    }
}