//
//  ModelViewController.m
//  OSGSampleIOS
//
//  Created by Matheus Coelho Berger on 9/14/16.
//
//

#import "ModelViewController.h"
#include "MultiTouchManipulator.hpp"

#include <osgGA/MultiTouchTrackballManipulator>
#include <osgGA/OrbitManipulator>
#include <osg/ShapeDrawable>

//include the iphone specific windowing stuff
#include <osgViewer/api/IOS/GraphicsWindowIOS>

#include "IfcPPReaderSTEP.h"
#include "GeometryConverter.h"
#include <osgUtil/Optimizer>


#include <osgDB/Registry>
#include <string>
#include <stdio.h>
#include <iostream>

@interface ModelViewController ()

@end

@implementation ModelViewController

- (BOOL)shouldAutorotate
{
    return YES;
}

   //@synthesize _window;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"criou o viewController");
    
    
    //get the screen size
    CGRect lFrame = [[UIScreen mainScreen] bounds];
    unsigned int w = lFrame.size.width * [[UIScreen mainScreen] scale];
    unsigned int h = lFrame.size.height  * [[UIScreen mainScreen] scale];
    
    NSLog(@"%dx%d", h, w);
    
    //create the viewer
    _viewer = new osgViewer::Viewer();
    

    if(1) {
        
        // If you want full control over the graphics context / window creation, please uncomment this section
        
        // create the main window at screen size
        self->_window = [[UIWindow alloc] initWithFrame: lFrame];
        
        //show window
        [_window makeKeyAndVisible];
        
        UIView* parent_view = [[UIView alloc] initWithFrame: CGRectMake(0,0, w, h)];
        parent_view.backgroundColor = [UIColor whiteColor];
        [self->_window addSubview: parent_view];
        self->_window.rootViewController = self;
        
        //create and set our graphics context following the frame (w,h)
        [self setOSGGraphicsContextWithWidth:w andHeight:h];
    }
    
    //ler arquivo ifc
    NSString * filepath = [[NSBundle mainBundle] pathForResource:@"arquivo" ofType:@"ifc"];
    [self loadIFCModel:filepath];
    //configurar camera osg
    [self configureOSGCameraWithWidth:w andHeight:h];
    
    // sun single-threaded
    _viewer->setThreadingModel(osgViewer::Viewer::SingleThreaded);
    
    _viewer->realize();
    
    // render a frame so the window-manager shows some content and not only an empty + black window
    _viewer->frame();
    
    
    // create a display link, which will update our scene on every screen-refresh
    
    _displayLink = [self.view.window.screen displayLinkWithTarget:self selector:@selector(updateScene)];
    [_displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//create our graphics context for OSG
- (void)setOSGGraphicsContextWithWidth: (unsigned int)w andHeight:(unsigned int) h
{
    //create our graphics context directly so we can pass our own window
    osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
    
    // Init the Windata Variable that holds the handle for the Window to display OSG in.
    osg::ref_ptr<osg::Referenced> windata = new osgViewer::GraphicsWindowIOS::WindowData(self.view);
    
    // Setup the traits parameters
    traits->x = 50;
    traits->y = 50;
    traits->width = w-100;
    traits->height = h-100;
    traits->depth = 16;     //keep memory down, default is currently 24
    traits->windowDecoration = false;
    traits->doubleBuffer = true;
    traits->sharedContext = 0;
    traits->setInheritedWindowPixelFormat = true;
    traits->samples = 4;
    traits->sampleBuffers = 1;
    
    traits->inheritedWindowData = windata;
    
    // Create the Graphics Context
    osg::ref_ptr<osg::GraphicsContext> graphicsContext = osg::GraphicsContext::createGraphicsContext(traits.get());
    
    // if the context was created then attach to our viewer
    if(graphicsContext)
    {
        _viewer->getCamera()->setGraphicsContext(graphicsContext);
        _viewer->getCamera()->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
    }
}

- (void)loadIFCModel:(NSString*) filepath
{
    //transformando nsstring em std::string
    std::string bar = std::string([filepath UTF8String]);
    //transformando std::string em std::wstring
    const std::wstring path(bar.begin(), bar.end());
    
    
    IfcPPReaderSTEP reader;
    
    shared_ptr<IfcPPModel> model(new IfcPPModel);
    
    //carregando modelo ifc do arquivo.ifc
    reader.loadModelFromFile(path, model);
    
    //debug
    std::string teste(model->getFileName().begin(), model->getFileName().end());
    std::cout << teste << std::endl;
    
    
    GeometryConverter geometryConverter(model);
    
    osg::ref_ptr<osg::Switch> parent_group = new osg::Switch();
    //
    geometryConverter.createGeometryOSG(parent_group);
    
    
    
    osg::ref_ptr<osg::Node> node = parent_group.get();
    
    osgUtil::Optimizer opt;
    opt.optimize(node);
    
    //create root
    _root = new osg::MatrixTransform();
    
    //load and attach scene model
    if (node) {
        printf("carregou o arquivo");
        
        _root->addChild(node);
    }
    else {
        osg::Geode* geode = new osg::Geode();
        osg::ShapeDrawable* drawable = new osg::ShapeDrawable(new osg::Box(osg::Vec3(1,1,1), 1));
        geode->addDrawable(drawable);
        _root->addChild(geode);
    }
}

- (void)configureOSGCameraWithWidth: (unsigned int)w andHeight:(unsigned int) h
{
    osg::Camera* hud_camera = createHUD(w,h);
    
    _root->addChild(hud_camera);
    
    _viewer->setSceneData(_root.get());
    _viewer->setCameraManipulator(new osgGA::MultiTouchManipulator());
    //_viewer->setCameraManipulator(new osgGA::OrbitManipulator());
    //_viewer->addEventHandler(new TestMultiTouchEventHandler(hud_camera));
    
    //My Fixing
    _viewer->getCamera()->setClearColor(osg::Vec4(1,1,1,0));
    _viewer->getCamera()->setProjectionMatrixAsPerspective(45.0f, float(w)/float(h), 0.05, 1000);
}

- (void)updateScene {
    _viewer->frame();
}

osg::Camera* createHUD(unsigned int w, unsigned int h)
{
    // create a camera to set up the projection and model view matrices, and the subgraph to draw in the HUD
    osg::Camera* camera = new osg::Camera;
    
    // set the projection matrix
    //camera->setProjectionMatrix(osg::Matrix::ortho2D(0,w,0,h));
    
    std::cout << w << " " << h << std::endl;
    std::cout << "\nratio: " << float(w)/float(h) << std::endl;
    
    // set the view matrix
    camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
    camera->setViewMatrix(osg::Matrix::identity());
    
    // only clear the depth buffer
    camera->setClearMask(GL_DEPTH_BUFFER_BIT);
    
    // draw subgraph after main camera view.
    camera->setRenderOrder(osg::Camera::POST_RENDER);
    
    // we don't want the camera to grab event focus from the viewers main camera(s).
    camera->setAllowEventFocus(false);
    
    
    // add to this camera a subgraph to render
    {
        std::cout << "adding camera to subgraph render" << std::endl;
        
        osg::Geode* geode = new osg::Geode();
        
        std::string timesFont("fonts/arial.ttf");
        
        // turn lighting off for the text and disable depth test to ensure it's always ontop.
        osg::StateSet* stateset = geode->getOrCreateStateSet();
        stateset->setMode(GL_LIGHTING,osg::StateAttribute::OFF);
        
        osg::Vec3 position(50.0f,h-50,10.0f);
        
        {
            std::cout << "adding text to screen" << std::endl;
            osgText::Text* text = new  osgText::Text;
            geode->addDrawable( text );
            
            text->setFont(timesFont);
            text->setPosition(position);
            text->setText("A simple multi-touch-example\n1 touch = rotate, \n2 touches = drag + scale, \n3 touches = home");
        }
        
        camera->addChild(geode);
    }
    
    return camera;
}

- (void)dealloc {
    _root = NULL;
    _viewer = NULL;
    [super dealloc];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
