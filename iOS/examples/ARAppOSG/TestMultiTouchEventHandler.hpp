//
//  TestMultiTouchEventHandler.hpp
//  OSGSampleIOS
//
//  Created by Matheus Coelho Berger on 9/14/16.
//
//

#ifndef TestMultiTouchEventHandler_hpp
#define TestMultiTouchEventHandler_hpp

#include "osgPlugins.h"

#include <osg/ShapeDrawable>
#include <osg/MatrixTransform>
#include <osgText/Text>
#include <osgGA/MultiTouchTrackballManipulator>
#include <stdio.h>

class TestMultiTouchEventHandler : public osgGA::GUIEventHandler {
public:
    TestMultiTouchEventHandler(osg::Group* parent_group)
    :   osgGA::GUIEventHandler(),
    _cleanupOnNextFrame(false)
    {
        createTouchRepresentations(parent_group, 10);
    }
    
private:
    void createTouchRepresentations(osg::Group* parent_group, unsigned int num_objects);
    virtual bool handle (const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa, osg::Object *, osg::NodeVisitor *);
    
    void cleanup(unsigned int j)
    {
        for(unsigned k = j; k < _mats.size(); ++k) {
            _mats[k]->setNodeMask(0x0);
        }
    }
    
    std::vector<osg::ShapeDrawable*> _drawables;
    std::vector<osg::MatrixTransform*> _mats;
    std::vector<osgText::Text*> _texts;
    bool _cleanupOnNextFrame;
    
};


#endif /* TestMultiTouchEventHandler_hpp */
