// Created by Thomas Hogarth 2009
// cleaned up by Stephan Huber 2013
//

// this example will create a fullscreen window showing a grey box. You can interact with it via
// multi-touch gestures.

#import "AppOSGDelegate.h"

@implementation AppOSGDelegate

//
//Called once app has finished launching
//
- (void)applicationDidFinishLaunching:(UIApplication *)application {
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}


-(void)applicationWillTerminate:(UIApplication *)application {
    /*if (_displayLink)
        [_displayLink invalidate];
    _displayLink = NULL;
    _root = NULL;
    _viewer = NULL;*/
} 

@end
