#ifndef MultiTouchManipulator_hpp
#define MultiTouchManipulator_hpp

#include <stdio.h>
#include <osgGA/TrackballManipulator>

namespace osgGA {
    
    
    class OSGGA_EXPORT MultiTouchManipulator : public TrackballManipulator
    {
        typedef TrackballManipulator inherited;
        
    public:
        
        MultiTouchManipulator( int flags = DEFAULT_SETTINGS );
        MultiTouchManipulator( const MultiTouchManipulator& tm,
                                       const osg::CopyOp& copyOp = osg::CopyOp::SHALLOW_COPY );
        
        META_Object( osgGA, MultiTouchManipulator );
        
        bool handle( const GUIEventAdapter& ea, GUIActionAdapter& us );
        
    protected:
        
        void handleMultiTouchDrag(GUIEventAdapter::TouchData* now, GUIEventAdapter::TouchData* last, const double eventTimeDelta);
        
        osg::ref_ptr<GUIEventAdapter::TouchData> _lastTouchData;
    };
    
    
}

#endif
