//
//  ModelViewController.h
//  OSGSampleIOS
//
//  Created by Matheus Coelho Berger on 9/14/16.
//
//

#include "osgPlugins.h"

#include <osgDB/ReadFile>
#include <osg/MatrixTransform>
#include <osgText/Text>
#include <osgViewer/Viewer>
#include "TestMultiTouchEventHandler.hpp"


#import <UIKit/UIKit.h>

@interface ModelViewController : UIViewController <UIAccelerometerDelegate> {

    UIWindow* _window; //main application window

    CADisplayLink* _displayLink;

    osg::ref_ptr<osgViewer::Viewer> _viewer;
    osg::ref_ptr<osg::MatrixTransform> _root;

}

//@property (nonatomic, retain) /*IBOutlet*/ UIWindow *_window;

- (void)updateScene;

@end
